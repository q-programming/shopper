Shopper
=========================================
Latest build: [![CircleCI](https://circleci.com/gh/q-programming/shopper.svg?style=svg)](https://circleci.com/gh/q-programming/shopper)

About
---
Application to create and share shopping lists.

Possible to login only using facebook or google (OAuth2)

REST API also allows to login using basic auth with password generated on first OAuth login 

To install deploy on tomcat, potentially create h2 database in tomcat run dir: 
./db/h2databaseFile

Stack: Spring Boot + Angular 6.x

Licence
----------
This application was created only be me , if you would like to change something , please notify me . I would love to see it :) All application is under GNU GPL License and uses some components under Apache License

Please note that application is not unique, there are other application similar in both behaviour and look ( angular material)
Purpose of this application was only for my usage and self development
